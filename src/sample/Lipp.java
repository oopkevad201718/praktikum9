package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Lipp extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    Group root = new Group();

    Rectangle blue = new Rectangle(300, 150, Color.BLUE);
    Rectangle black = new Rectangle(300, 150, Color.BLACK);
    Rectangle white = new Rectangle(300, 150, Color.WHITE);

    black.setY(150);
    white.setY(300);

    root.getChildren().addAll(blue, black, white);
    Scene scene = new Scene(root, 300, 450, Color.SNOW);

    scene.widthProperty().addListener(new ChangeListener<Number>() {
      @Override public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        blue.setWidth((Double) newValue);
        black.setWidth((Double) newValue);
        white.setWidth((Double) newValue);
      }
    });

    scene.heightProperty().addListener(new ChangeListener<Number>() {
      @Override public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        double height = (Double) newValue / 3;
        blue.setHeight(height);

        black.setHeight(height);
        black.setY(height);

        white.setHeight(height);
        white.setY(height * 2);
      }
    });

    stage.setTitle("Lipp");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
