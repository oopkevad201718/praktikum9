package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Hiir extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    Group root = new Group();

    Scene scene = new Scene(root, 300, 150, Color.SNOW);

    Label label = new Label("(0, 0)");

    // Hetkel me näitame koordinaate koguaeg, ülesandes oli kirjas, et
    // peaks näitama hiire vajutusel ja lahti laskmisel peaks tekst ära kaduma
    // Kui aega saate võite ümber proovida teha.
    scene.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
      public void handle(MouseEvent me) {
        label.setTranslateX(me.getX());
        label.setTranslateY(me.getY() - 10);
        label.setText("(" + me.getX() + ", " + me.getY() + ")");
      }
    });


    root.getChildren().add(label);

    stage.setTitle("Lipp");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
